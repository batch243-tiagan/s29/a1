db.users.insert({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "12345678",
			email: "jandedoe@gmail.com"
		},
		courses: [ "CSS", "JavaScript", "Python"],
		department: "none"
	});

db.users.insertMany([{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		},
		courses:[ "Python", "React", "PHP"],
		department: "none"
	}, {
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "87654321",
			email: "neilarmstrong@gmail.com"
		},
		courses: ["React", "Laravel", "Sass"],
		department: "none"
	}]);

	db.users.insert({
		firstName: "Test",
		lastName: "Test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		course: [],
		department: "none"
	});

	db.users.updateOne(
		{ firstName: "Bill"},
		{
			$set:{
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	);

	db.users.updateMany({department: "none"}, {
		$set: {
			department: "HR"
		}
	});

	db.users.replaceOne({firstName :"Bill"},
			{
					firstName: "Chris",
					lastName: "Mortel",
					age: 14,
					contact: {
							phone:"12345678",
							email: "chris@gmail.com"
						},
					courses:["PHP", "Laravel", "HTML"],
					department: "Operations"
		
			}
	);


	db.users.insert({
		firstName: "test"
	});

	db.users.deleteOne({firstName: "test"});
